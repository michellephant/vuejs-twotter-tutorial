export const users = [
  {
    "id": 1,
    "username": "_TwotterHandle",
    "firstName": "Twotter",
    "lastName": "Handle",
    "email": "testemail@test.com",
    "isAdmin": true,
    "twoots": [
      { "id": 1, "content": "Twotter is amazing!" },
      { "id": 2, "content": "Don't forget to subscribe!" }
    ]
  },
  {
    "id": 2,
    "username": "_FakePerson",
    "firstName": "Fake",
    "lastName": "Person",
    "email": "fakeperson@test.com",
    "isAdmin": true,
    "twoots": []
  },
  {
    "id": 3,
    "username": "_JaneDoe",
    "firstName": "Jane",
    "lastName": "Doe",
    "email": "janedoe@test.com",
    "isAdmin": true,
    "twoots": []
  },
  {
    "id": 3,
    "username": "_JohnDoe",
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@test.com",
    "isAdmin": true,
    "twoots": []
  }
]